<?php

require 'database.php';
$zodiac_sign= $_REQUEST['zodiac_sign'];
//$prediction_mode= $_REQUEST['mode'];
$prediction_mode= 1;
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$today_date=$db->getTodayDate();
	if ($result = $mysqli->query("SELECT `prediction`.`general`,`prediction`.`work`,`prediction`.`love`,`prediction`.`health`,`quotes`.`quote`,`auspicious_hour`.`auspicious_hour`,`auspicious_hour`.`inauspicious_hour`,`lucky_colour`.`lucky_colour`,`lucky_number`.`lucky_number` FROM `prediction`,`quotes`,`auspicious_hour`,`lucky_colour`,`lucky_number` WHERE `prediction`.`zodiac_sign_id`='$zodiac_sign' AND `prediction`.`mode`='$prediction_mode' AND `prediction`.`start_date`='$today_date' AND `quotes`.`zodiac_sign_id`='$zodiac_sign' AND `quotes`.`start_date`='$today_date' AND `auspicious_hour`.`zodiac_sign_id`='$zodiac_sign' AND `auspicious_hour`.`date`='$today_date' AND `lucky_colour`.`zodiac_sign_id`='$zodiac_sign' AND `lucky_colour`.`start_date`='$today_date' AND `lucky_number`.`zodiac_sign_id`='$zodiac_sign' AND `lucky_number`.`start_date`='$today_date'")) 
	{
		/* determine number of rows result set */
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message='Something went wrong. Please try again later.';
		}else{
			$error=0;
			$result_arr=$result->fetch_array(MYSQLI_ASSOC);
		}
		/* close result set */
		$result->close();
	}else{
		$error=1;
		$message='Something went wrong. Please try again later.';
	}
	/* close connection */
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	echo json_encode($response_arr);
}

?>