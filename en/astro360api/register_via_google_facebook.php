<?php

require 'database.php';
$name=$_REQUEST['name'];
$email= $_REQUEST['email'];
$dob= $_REQUEST['dob'];
$device_id=$_REQUEST['device_id'];
$is_facebook_user=$_REQUEST['is_facebook_user'];
$is_google_user=$_REQUEST['is_google_user'];
$facebook_or_google_user_id=$_REQUEST['facebook_google_user_id'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$timestamp=$db->getCurrentDateTime();
	$is_user_exist_query="";
	$user_insert_query="";
	if(strcmp($is_facebook_user,"1") and strcmp($is_google_user,"0")){
		$is_user_exist_query="SELECT `id` FROM `user` WHERE `facebook_or_google_user_id`='$facebook_or_google_user_id' AND `facebook_user`='$is_facebook_user'";
		$user_insert_query="INSERT INTO `user`(`email`, `full_name`, `dob`, `mac_address`, `timestamp`, `facebook_user`,`facebook_or_google_user_id`, `google_user`) VALUES ('$email', '$name', '$dob', '$device_id', '$timestamp', '$is_facebook_user', '$facebook_or_google_user_id','$is_google_user')";
	}
	if(strcmp($is_google_user,"1") and strcmp($is_facebook_user,"0")){
		$is_user_exist_query="SELECT `id` FROM `user` WHERE `facebook_or_google_user_id`='$facebook_or_google_user_id'  AND `facebook_user`='$is_facebook_user'";
		$user_insert_query="INSERT INTO `user`(`email`, `full_name`, `dob`, `mac_address`, `timestamp`, `google_user`,`facebook_or_google_user_id`, `facebook_user`) VALUES ('$email', '$name', '$dob', '$device_id', '$timestamp', '$is_google_user', '$facebook_or_google_user_id','$is_facebook_user')";
	}
	if(empty($is_user_exist_query) and empty($user_insert_query)){
		$error=1;
		$message='Not able to build select and insert query.';
	}else{
		if ($result = $mysqli->query($is_user_exist_query)) 
		{
			if(mysqli_num_rows($result)==0)
			{
				if ($mysqli->query($user_insert_query) === TRUE) {
					$error=0;
					$message=$mysqli->insert_id;
				} else {
					$error=1;
					$message="Error: " . $sql . "<br>" . $mysqli->error;
				}
			}
			else
			{
				$row=$result->fetch_array(MYSQLI_ASSOC);
				$user_id=$row['id'];
				$user_update_query=" UPDATE `user` SET ";
				if(!empty($name))
				{
					$user_update_query.=" `full_name`='$name' ";
				}
				if(!empty($email))
				{
					$user_update_query.=", `email`='$email' ";
				}
				if(!empty($dob))
				{
					$user_update_query.=", `dob`='$dob' ";
				}
				if(!empty($device_id))
				{
					$user_update_query.=", `mac_address`='$device_id' ";
				}
				$user_update_query.=" WHERE `id`='$user_id' ";
				$mysqli->query($user_update_query);
				$error=0;
				$message=$user_id;	
			}
			$result->close();
		}
		$mysqli->close();
	}
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	echo json_encode($response_arr);
}

?>