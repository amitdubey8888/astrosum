<?php

require 'database.php';
$user_id= $_REQUEST['user_id'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$today_date=$db->getTodayDate();
	
	if ($result = $mysqli->query("SELECT `revenue`,`package`, `timestamp` FROM `postback` WHERE `device`=(SELECT `app_next_device_id` FROM `user` WHERE `id`=$user_id)")) 
	{
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message='Go on Add Credit to earn Astro Point.';
		}else{
			$error=0;
			while($row=$result->fetch_array(MYSQLI_ASSOC))
			{
				$result_arr[]=$row;
			}
		}
		$result->close();
	}
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	echo json_encode($response_arr);
}

?>