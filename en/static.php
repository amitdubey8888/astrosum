<?php

require 'database.php';
$title= $_REQUEST['title'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$today_date=$db->getTodayDate();
	
	if ($result = $mysqli->query("SELECT `content` FROM `static_page` WHERE `title`='$title'"))
	{
		/* determine number of rows result set */
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message='Something went wrong. Please try again later.';
		}else{
			$error=0;
			while($row=$result->fetch_array(MYSQLI_ASSOC))
			{
				$result_arr[]=$row;
			}
		}
		/* close result set */
		$result->close();
	}
	/* close connection */
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	echo json_encode($response_arr);
}

?>