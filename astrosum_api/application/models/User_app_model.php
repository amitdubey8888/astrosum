<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_app_model extends CI_Model{





//user login 
public function login_check($mob)
{
   $this ->db-> select('*');
   $this ->db-> from('users');
   $this ->db-> where('mobile', $mob);
   $this ->db-> where('user_type', 'User');
   $this ->db-> where('status','active');
   
   $this ->db-> limit(1);
 
   $query = $this->db-> get();
 
   if($query -> num_rows() == 1)
   {
     return $query->result();
   }
   else
   {
     return false;
   }
}


public function check_mob($mob)
{
    $this ->db-> select('*');
   $this ->db-> from('users');
   $this ->db-> where('mobile', $mob);
   $this ->db-> where('user_type', 'User');
  
   $this ->db-> limit(1);
 
   $query = $this->db-> get();
 
   if($query -> num_rows() == 1)
   {
    
    return false;   
   }
   else 
   {
       return true; 
   }
}

//check mob no. at login time 
public function check_mobsign($mob)
{
    $this ->db-> select('*');
   $this ->db-> from('users');
   $this ->db-> where('mobile', $mob);
   $this ->db-> where('user_type', 'User');
  
   $this ->db-> limit(1);
 
   $query = $this->db-> get();
 
   if($query -> num_rows() == 1)
   {
    
    return true;   
   }
   else 
   {
       return false; 
   }
}



//user singup 
public function singup_user($name, $mob, $aadhar , $values, $address)
{
    $this ->db-> select('*');
   $this ->db-> from('users');
   $this ->db-> where('mobile', $mob);
   $this ->db-> where('user_type', 'User');
  
   $this ->db-> limit(1);
 
   $query = $this->db-> get();
   if($query -> num_rows() == 1)
   {
    
    return false;   
   }
   else
   {
     $data=$this->db->insert('users', $values);
    if($data)
    {
        $query=$this->db->query("select * from users where mobile='$mob' AND user_type='User' AND status='active'");
        $uid=$query->row()->id;
        
         $data=$this->db->query("insert into payment_mode (userid, address) value('$uid','$address')");
        
        
        
     return $query->result_array();
    }
    else 
    {
        return false;
    }
   }
    

}

//get state list at signup time 
public function get_state()
{
    $query=$this->db->query("select * from state ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}

//get state list at signup time 
public function get_city()   //$stateid)
{
    $query=$this->db->query("select * from city ");// where state_id='$stateid' ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}




//get panchite list at signup time 
public function get_panchite($cityid)
{
    $query=$this->db->query("select * from panchayat where city_id='$cityid' ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}




//get city list at signup time 
public function get_grampanchite($pid)
{
    $query=$this->db->query("select * from grampanchayat where panchayat_id='$pid' ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}




// get franchise list 
public function get_franchise($cityid, $panchiteid, $grampanchiteid)
{
    $query=$this->db->query("select * from franchices where city_id='$cityid' AND panchayat_id='$panchiteid' AND gram_id='$grampanchiteid' ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}



//  all category get for home screen 
public function get_allcat()
{
    $query=$this->db->query("select * from category ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}



//get user profile 
public function get_userpro($userid)
{
    $query=$this->db->query("select * from users where id='$userid' ");
    if($query)
    {
        return $query->result_array();
    }
    else 
    {
        return false;
    }
}




//get user profile address 
public function get_userproadd($userid, $cityid, $addtype, $panid, $grampanid)
{
    
    if($addtype=="urban")
    {
        
        $query=$this->db->query("select city_name from city where city_id='$cityid' ");
        $cityname=$query->row()->city_name;
        return $cityname;
    }
    else 
    {
        return false;
    }
    
    
    
}



//get all sub category at order time 

public function readcat($cat)
{
  $query = $this->db->query("select * from sub_category  where cat_id='$cat' ");
  if($query)
  {
     return $query->result_array(); 
  }
  else 
  {
      return false;
  }

}




//get all order list 
public function getallorder($userid)
{
  $query = $this->db->query("select * from orders where user_id='$userid' ");
  if($query)
  {
     return $query->result_array(); 
  }
  else 
  {
      return false;
  }

}


//get notification at user conformation time 
public function getnotification($userid)
{
  $query = $this->db->query("select * from orders where user_id='$userid' AND finalstatus='verified'" );
  if($query)
  {
     return $query->result_array(); 
  }
  else 
  {
      return false;
  }

}



public function postpaysystem($userid, $orderid, $pay_type, $amount, $addid)
{
   $query = $this->db->query("UPDATE orders SET addressid='$addid' WHERE order_id='$orderid'" );
   $data=$this->db->query("insert into payment_mode (orderid, userid, payment_type, amount) value('$userid','$orderid','$pay_type','$amount')");
   $query1 = $this->db->query("select * from payment_mode where userid='$userid' AND orderid='$orderid'" );
  if($query1)
  {
     return $query1->result_array(); 
  }
  else 
  {
      return false;
  } 
}




public function postorderdinay( $orderid, $resition )
{
   $query = $this->db->query("UPDATE orders SET deny_reason='$resition', finalstatus='decline' WHERE order_id='$orderid'" );
   
  if($query)
  {
     return true; 
  }
  else 
  {
      return false;
  } 
}



public function getalladdress( $userid )
{
   $query = $this->db->query("select * from user_address WHERE userid='$userid'" );
   
  if($query)
  {
     return $query->result_array(); 
  }
  else 
  {
      return false;
  } 
}


public function postpostaddress( $userid, $address )
{
       $data=$this->db->query("insert into user_address (userid, address) value('$userid','$address')");
       $query = $this->db->query("select MAX(id) as id from user_address" );
   
  if($query)
  {
     return $query->result_array(); 
  }
  else 
  {
      return false;
  } 
}




public function orderconformed( $userid, $orderid, $address, $paytype, $amount )
{
    $data=$this->db->query("insert into payment_mode (userid, orderid, payment_type, amount) value('$userid','$orderid', '$paytype','$amount')");
       $query = $this->db->query("UPDATE orders SET addressid='$address', finalstatus='confirmed' WHERE order_id='$orderid'" );
   
  if($query && $data)
  {
     return true; 
  }
  else 
  {
      return false;
  } 
}



//test test 
public function insert11($data){ 
$this->db->insert('testapi', $data);
return TRUE;
}

//test test 
public function get11(){ 
$data=$this->db->query("select * from testapi");
if($data)
{
    return $data->result_array();
}
else 
{
    return false;
}
}




}