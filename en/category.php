<?php

require 'database.php';
$category_id= $_REQUEST['category_id'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$today_date=$db->getTodayDate();

	if ($result = $mysqli->query("SELECT `description`,`user_require`,`points_require` FROM `category` WHERE `id`='$category_id'")) 
	{
		/* determine number of rows result set */
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message='Something went wrong. Please try again later.';
		}else{
			$error=0;
			$result_arr=$result->fetch_array(MYSQLI_ASSOC);
		}
		/* close result set */
		$result->close();
	}
	/* close connection */
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	print json_encode($response_arr);
}

?>