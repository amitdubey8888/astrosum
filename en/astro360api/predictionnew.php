<?php

require 'database.php';
$zodiac_sign= $_REQUEST['zodiac_sign'];
$prediction_mode= $_REQUEST['mode'];
$today_date= $_REQUEST['date'];
$error=0;
$message='';
$result_arr = array();

try{
	$db = Database::getInstance();
	$mysqli = $db->getConnection();
	$select_query="";
	if($prediction_mode==1)
	{
		$select_query="SELECT `prediction`.`general_prediction`, `prediction`.`health`, `prediction`.`love`, `prediction`.`work`, `auspicious_hour`.`auspicious_hour`, `auspicious_hour`.`inauspicious_hour`, `lucky_colour`.`lucky_colour`, `lucky_number`.`lucky_number`, `quotes`.`quote` FROM `prediction`, `auspicious_hour`, `lucky_colour`, `lucky_number`, `quotes` WHERE `prediction`.`zodiac_sign_id`=`auspicious_hour`.`zodiac_sign_id` AND `prediction`.`mode`=`auspicious_hour`.`mode` AND `prediction`.`start_date`=`auspicious_hour`.`date` AND `prediction`.`zodiac_sign_id`=`lucky_colour`.`zodiac_sign_id` AND `prediction`.`mode`=`lucky_colour`.`mode` AND `prediction`.`start_date`=`lucky_colour`.`start_date` AND `prediction`.`zodiac_sign_id`=`lucky_number`.`zodiac_sign_id` AND `prediction`.`mode`=`lucky_number`.`mode` AND `prediction`.`start_date`=`lucky_number`.`start_date` AND `prediction`.`zodiac_sign_id`=`quotes`.`zodiac_sign_id` AND `prediction`.`mode`=`quotes`.`mode` AND `prediction`.`start_date`=`quotes`.`start_date` AND `prediction`.`zodiac_sign_id`='$zodiac_sign' AND `prediction`.`mode`='$prediction_mode' AND `prediction`.`start_date`='$today_date'";
	}
	if($prediction_mode==7)
	{
		$select_query="SELECT `prediction`.`general_prediction`, `prediction`.`health`, `prediction`.`love`, `prediction`.`work`, `lucky_colour`.`lucky_colour`, `lucky_number`.`lucky_number`, `quotes`.`quote` FROM `prediction`, `lucky_colour`, `lucky_number`, `quotes` WHERE `prediction`.`zodiac_sign_id`=`lucky_colour`.`zodiac_sign_id` AND `prediction`.`mode`=`lucky_colour`.`mode` AND `prediction`.`start_date`=`lucky_colour`.`start_date` AND `prediction`.`end_date`=`lucky_colour`.`end_date` AND `prediction`.`zodiac_sign_id`=`lucky_number`.`zodiac_sign_id` AND `prediction`.`mode`=`lucky_number`.`mode` AND `prediction`.`start_date`=`lucky_number`.`start_date` AND `prediction`.`end_date`=`lucky_number`.`end_date` AND `prediction`.`zodiac_sign_id`=`quotes`.`zodiac_sign_id` AND `prediction`.`mode`=`quotes`.`mode` AND `prediction`.`start_date`=`quotes`.`start_date` AND `prediction`.`end_date`=`quotes`.`end_date` AND `prediction`.`zodiac_sign_id`='$zodiac_sign' AND `prediction`.`mode`='$prediction_mode' AND DATEDIFF(`prediction`.`end_date`, STR_TO_DATE('$today_date','%Y-%m-%d')) <=6 AND DATEDIFF(`prediction`.`end_date`, STR_TO_DATE('$today_date','%Y-%m-%d')) >=0 AND DATEDIFF(`prediction`.`start_date`, STR_TO_DATE('$today_date','%Y-%m-%d')) <=0 AND DATEDIFF(`prediction`.`start_date`, STR_TO_DATE('$today_date','%Y-%m-%d')) >= -6";
	}
	if ($result = $mysqli->query($select_query)) 
	{
		/* determine number of rows result set */
		if(mysqli_num_rows($result)==0)
		{			
			$error=1;
			$message='Data not available on server, Sorry for inconvenience.';
		}else{
			$result_arr=$result->fetch_array(MYSQLI_ASSOC);
		}
		/* close result set */
		$result->close();
	}
	else
	{
		$error=1;
		$message='Data not available on server, Sorry for inconvenience.';
	}	
	/* close connection */
	$mysqli->close();
}catch(Exception $e1){
	$error=1;
	$message=$e1->getMessage();
}finally{
	$response_arr=array('error'=>$error,'message'=>$message,'result'=>$result_arr);
	die (json_encode($response_arr));
}

?>