<?php
/*
* Mysql database class - only one connection alowed
*/
class Database {
	private $_connection;
	private static $_instance; //The single instance
	private $_host = "localhost";
	private $_username = "astro360_rajeev";
	private $_password = "rajeev#111";
	private $_database = "astro360_n";

	/*
	Get an instance of the Database
	@return Instance
	*/
	public static function getInstance() {
		if(!self::$_instance) { // If no instance then make one
			self::$_instance = new self();
		}
		return self::$_instance;
	}
	
	public static function getCurrentDateTime() {
		$now = new DateTime();
		$now->getTimestamp();
		return $now->format('Y-m-d H:i:s');
	}
	
	public static function getTodayDate() {
		$indiatimezone = new DateTimeZone("Asia/Kolkata");
		$date=new DateTime();
		$date->setTimezone($indiatimezone);
		return $date->format('Y-m-d');
	}

	// Constructor
	private function __construct() {
		$this->_connection = new mysqli($this->_host, $this->_username, 
			$this->_password, $this->_database);
	
		// Error handling
		if(mysqli_connect_error()) {
			trigger_error("Failed to conencto to MySQL: " . mysql_connect_error(),
				 E_USER_ERROR);
		}
	}

	// Magic method clone is empty to prevent duplication of connection
	private function __clone() { }

	// Get mysqli connection
	public function getConnection() {
		return $this->_connection;
	}
}
?>